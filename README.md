# C4 Gateway

Forward your request to C4 services.

## Run

### With Node.js

You need to install Node-RED then you can run the gateway.

```
npm install -g node-red
```

Then run the project:

```
env BACKENDS="http://127.0.0.1:8000,http://localhost:8002" ENV=dev node-red -p 1880 -u .
```

### With Docker

### Environment variable

* `BACKENDS`: the mandatory list of commas separated backends URL
* `NO_ADMIN`: if set disable the Node-RED admin UI interface

## Build

## Test

Run the test

## Configuration endpoint

You can change the gateway's backends without restart the service with the configuration endpoint:

```
curl --location 'http://{c4-gateway}/config' \
--header 'Content-Type: application/json' \
--data '{
    "backends": ["http://backend1", "http://backend2"]
}'
```