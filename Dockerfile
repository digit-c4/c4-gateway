FROM nodered/node-red:latest

COPY settings.js /data
COPY package.json /data
COPY flows.json /data