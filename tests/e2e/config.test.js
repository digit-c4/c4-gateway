const request = require("supertest");

const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT || 1880;

const app = `http://${HOST}:${PORT}`;

describe("Config endpoint", () => {
    test("that config is set", async () => {
        let response = await request(app).post("/config").send({
            backends: [
                "http://test1.com",
                "http://test2.com",
            ],
        });

        expect(response.statusCode).toEqual(200);

        response = await request(app).get("/config");

        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual({
            backends: [
                "http://test1.com",
                "http://test2.com",
            ],
        });
    });
});
