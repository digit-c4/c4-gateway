const request = require("supertest");
const { backend, server } = require("./backend");

const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT || 1880;
const BACKEND = process.env.HOSTNAME || "localhost";

const app = `http://${HOST}:${PORT}`;

backend
    .get("/get", (req, res) => {
        res.status(200)
            .set({
                method: req.headers.method,
            })
            .json({
                hello: "world",
            });
    })
    .post("/post", (req, res) => {
        res.status(201)
            .set({
                method: req.headers.method,
            })
            .send(req.body);
    })
    .put("/put", (req, res) => {
        res.status(200)
            .set({
                method: req.headers.method,
            })
            .send(req.body);
    })
    .patch("/patch", (req, res) => {
        res.status(200)
            .set({
                method: req.headers.method,
            })
            .send(req.body);
    })
    .delete("/delete", (req, res) => {
        res.status(200)
            .set({
                method: req.headers.method,
            }).send();
    });

beforeAll(async () => {
    server.listen({
        port: 8081,
    });

    await request(app).post("/config").send({
        backends: [
            `http://${BACKEND}:8081`,
        ],
    });
});

afterAll(() => {
    server.close();
});

describe("Method forward tests suite case", () => {
    test("that GET method works", async () => {
        const response = await request(app).get("/get").set("method", "GET").send();

        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual({
            hello: "world",
        });
        expect(response.headers.method).toEqual("GET");
    });

    test("that POST method works", async () => {
        const response = await request(app).post("/post").set("method", "POST").send({
            hello: "world",
        });

        expect(response.statusCode).toEqual(201);
        expect(response.body).toEqual({
            hello: "world",
        });
        expect(response.headers.method).toEqual("POST");
    });

    test("that PUT method works", async () => {
        const response = await request(app).put("/put").set("method", "PUT").send({
            hello: "world",
        });

        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual({
            hello: "world",
        });
        expect(response.headers.method).toEqual("PUT");
    });

    test("that PATCH method works", async () => {
        const response = await request(app).patch("/patch").set("method", "PATCH").send({
            hello: "world",
        });

        expect(response.statusCode).toEqual(200);
        expect(response.body).toEqual({
            hello: "world",
        });
        expect(response.headers.method).toEqual("PATCH");
    });

    test("that DELETE method works", async () => {
        const response = await request(app).delete("/delete").set("method", "DELETE").send();

        expect(response.statusCode).toEqual(200);
        expect(response.text).toHaveLength(0);
        expect(response.headers.method).toEqual("DELETE");
    });
});
