const express = require("express");
const http = require("http");

const backend = express();
backend.use(express.json());
const server = http.createServer(backend);

module.exports = {
    backend,
    server,
};
