const request = require("supertest");
const { backend, server } = require("./backend");

const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT || 1880;
const BACKEND = process.env.HOSTNAME || "localhost";

const app = `http://${HOST}:${PORT}`;

backend.post("/test", (req, res) => {
    res.status(201)
        .set({
            test: req.headers.test,
        })
        .send(req.body);
});

beforeAll(() => {
    server.listen({
        port: 8081,
    });
});

afterAll(() => {
    server.close();
});

describe("Backend forward tests suite case", () => {
    test("that fallback works", async () => {
        let response = await request(app).post("/config").send({
            backends: [
                `http://${BACKEND}:8080`,
                `http://${BACKEND}:8081`,
            ],
        });

        expect(response.statusCode).toEqual(200);

        response = await request(app).post("/test").set("test", "123456").send({
            hello: "world",
        });

        expect(response.statusCode).toEqual(201);
        expect(response.body).toEqual({
            hello: "world",
        });
        expect(response.headers.test).toEqual("123456");
    });
});
